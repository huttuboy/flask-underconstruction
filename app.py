from flask import Flask, render_template
from os import getenv


TITLE = getenv("CUSTOM_TITLE", "Under construction")
CONTENT = getenv("CUSTOM_CONTENT", "Under construction")

app = Flask(__name__)


@app.route('/')
def base():
    return render_template('index.html', title=TITLE, content=CONTENT)


if __name__ == '__main__':
    app.threaded = getenv("FLASK_THREADED", "true").lower() in ("true", "1")
    app.debug = getenv("FLASK_DEBUG", "false").lower() in ("true", "1")
    app.run(host='0.0.0.0', port=5000)
